package model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import setting.Levels;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static model.MinesweeperModel.mapOfRecord;

public class Record implements Serializable {
    private Levels level;
    private String name;
    private String time;

    private static Gson gson = new Gson();
    private static Type type = new TypeToken<HashMap<Levels, List<Record>>>(){}.getType();


    Record(String name, String time, Levels level) {
        this.name = name;
        this.time = time;
        this.level = level;
    }

    public Levels getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    String getTime() {
        return time;
    }

    static void loadRecords(){
        try {
            String data = Files.readString(Paths.get("records.txt"));
            if (!data.isEmpty()) {
                mapOfRecord = new Gson().fromJson(data, type);
            }
        } catch (IOException e) {
            System.err.println("can't read record");
        }
    }

    static void writeRecord(Map<Levels, List<Record>> mapOfRecord) {
        String jsonMap = gson.toJson(mapOfRecord, type);
        try {
            FileWriter fw = new FileWriter("records.txt");
            fw.write(jsonMap);
            fw.close();
        } catch (IOException e) {
            System.err.println("can't write record");
        }
    }

    @Override
    public String toString() {
        return name + " : " + time+"\n";
    }
}
