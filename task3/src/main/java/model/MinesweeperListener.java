package model;

public interface MinesweeperListener {
    void setCellOpened(int line, int column);

    void setCellDefault(int line, int column);

    void setCellFlag(int line, int column);

    void setCellMine(int line, int column);

    void openAllMine(MinesweeperCell[][] gameField);

    void WinOperation();

    void GameOverOperation();

    void setCellMineNearNeighbors(int line, int column, int countMineNeighbors);

    void setScreenSize(int line, int column);

    void restartScreen(int line, int column);

    void setGameTime(String seconds);

    void askNameDialog();

    void getRecord(String record);

}
