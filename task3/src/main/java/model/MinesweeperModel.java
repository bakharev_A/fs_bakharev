package model;

import setting.Levels;

import java.util.*;
import java.util.List;


public class MinesweeperModel {

    private int line;
    private int column;
    private int countFlags;
    private int countClosedCell;
    private int countMinesOnField;
    private boolean isGameStopped;
    private String seconds;
    private Levels level;

    private MinesweeperCell[][] gameField;
    private MinesweeperListener listener;

    static Map<Levels, List<Record>> mapOfRecord = Map.of(Levels.BEGINNER, new ArrayList<>(),
            Levels.MEDIUM, new ArrayList<>(), Levels.EXPERT, new ArrayList<>());


    public void setListener(MinesweeperListener listener) {
        this.listener = listener;
    }

    public void getRecord() {
        List<Record> recordList = mapOfRecord.get(level);
        if (recordList.isEmpty()) {
            listener.getRecord("Рекордов пока нет :(");
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            int i = 1;
            for (Record rec : recordList) {
                stringBuilder.append(i).append(") ").append(rec.toString());
                i++;
            }
            listener.getRecord(stringBuilder.toString());
        }
    }

    public void setRecord(String name) {
        List<Record> recordList = mapOfRecord.get(level);
        Record record = new Record(name, seconds, level);
        recordList.add(record);
        recordList.sort(Comparator.comparingInt(o -> Integer.parseInt(o.getTime())));
        if (recordList.size() > 3) {
            recordList.remove(3);
        }
        Record.writeRecord(mapOfRecord);
    }

    public void setTime(long seconds) {
        if (!isGameStopped) {
            this.seconds = String.valueOf(seconds);
            listener.setGameTime(String.valueOf(seconds));
        }
    }

    public void initializeCustomGame(int line, int column, int mine) {
        this.level = Levels.CUSTOM;
        this.line = line;
        this.column = column;
        this.countMinesOnField = mine;
        listener.setScreenSize(line, column);
        createGame();
    }

    public void restart() {
        isGameStopped = true;
        listener.setGameTime("");
        countClosedCell = line * column;
        createGame();
        listener.restartScreen(line, column);

    }

    public void initialize(Levels level) {

        this.level = level;
        line = level.getLine();
        column = level.getColumn();
        countMinesOnField = level.getMine();
        listener.setScreenSize(line, column);

        createGame();
        Record.loadRecords();
    }

    private void createGame() {
        isGameStopped = false;
        countFlags = countMinesOnField;
        countClosedCell = line * column;
        createCellField();
    }

    private void createCellField() {

        gameField = new MinesweeperCell[line][column];
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                gameField[i][j] = new MinesweeperCell(i, j, false);
            }
        }
        addMines();
        countMineNeighbors();

    }

    private void addMines() {
        int count = 0;
        //заполняем подряд минами
        all:
        while (true) {
            for (int i = 0; i < line; i++) {
                for (int j = 0; j < column; j++) {
                    gameField[i][j].mine = true;
                    count++;
                    if (count == countMinesOnField) {
                        break all;
                    }
                }
            }
        }
        // Миксуем мины
        Random r = new Random();
        int iRand;
        int jRand;
        boolean temp;

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                iRand = r.nextInt(line);
                jRand = r.nextInt(column);
                temp = gameField[i][j].mine;
                gameField[i][j].mine = gameField[iRand][jRand].mine;
                gameField[iRand][jRand].mine = temp;
            }
        }
    }

    private void countMineNeighbors() {

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                if (!gameField[i][j].mine) {
                    getCount(i, j);
                }
            }
        }
    }

    private void getCount(int i, int j) {
        List<MinesweeperCell> list;
        list = getCellNeighbors(gameField[i][j]);
        for (MinesweeperCell object : list) {
            if (object.mine) {
                gameField[i][j].countMineNeighbors++;
            }
        }
    }

    private List<MinesweeperCell> getCellNeighbors(MinesweeperCell cell) {
        ArrayList<MinesweeperCell> listOfNeighbors = new ArrayList<>();
        for (int i = cell.line - 1; i <= cell.line + 1; i++) {
            for (int j = cell.column - 1; j <= cell.column + 1; j++) {
                if ((i != cell.line || j != cell.column) && ((i >= 0) && (j >= 0) && (i < line) && (j < column))) {
                    listOfNeighbors.add(gameField[i][j]);
                }
            }
        }

        return listOfNeighbors;
    }


    public void markCell(int line, int column) {
        MinesweeperCell cell = gameField[line][column];
        if (isGameStopped) {
            return;
        }
        if (!cell.open && countFlags > 0 && !cell.flag) {
            cell.flag = true;
            listener.setCellFlag(line, column);
            countFlags--;

        } else if (!cell.open && cell.flag) {
            cell.flag = false;
            listener.setCellDefault(line, column);
            countFlags++;

        }
    }

    public void openCell(int line, int column) {
        MinesweeperCell cell = gameField[line][column];


        if (cell.open || cell.flag || isGameStopped) {
            return;
        }
        if (cell.mine) {
            listener.setCellMine(line, column);
            listener.openAllMine(gameField);
            gameOver();
        } else {
            open(cell);
        }
    }

    private void open(MinesweeperCell cell) {

        cell.open = true;
        countClosedCell--;

        if (countClosedCell == countMinesOnField) {
            win();
        }

        System.out.println(countClosedCell);

        if (cell.countMineNeighbors == 0) {
            openAllNeighbors(cell);
        } else {
            listener.setCellMineNearNeighbors(cell.line, cell.column, cell.countMineNeighbors);
        }
    }

    private void openAllNeighbors(MinesweeperCell cell) {
        listener.setCellOpened(cell.line, cell.column);

        List<MinesweeperCell> cells = getCellNeighbors(cell);
        for (MinesweeperCell obj : cells) {
            if (!obj.mine) {
                listener.setCellOpened(cell.line, cell.column);
                openCell(obj.line, obj.column);
            }
        }
    }

    private void win() {
        listener.WinOperation();
        isGameStopped = true;
        if (level == Levels.BEGINNER || level == Levels.MEDIUM || level == Levels.EXPERT) {
            listener.askNameDialog();
        }
    }

    private void gameOver() {
        listener.GameOverOperation();
        isGameStopped = true;
    }
}

