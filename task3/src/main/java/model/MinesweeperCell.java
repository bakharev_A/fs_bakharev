package model;

public class MinesweeperCell {

    int line;
    int column;
    boolean mine;
    boolean open;
    boolean flag;

    int countMineNeighbors = 0;

    MinesweeperCell(int line, int column, boolean mine) {
        this.line = line;
        this.column = column;
        this.mine = mine;
    }

    public boolean isMine() {
        return mine;
    }
}
