import controller.MinesweeperController;

import model.MinesweeperModel;
import view.MinesweeperView;



public class Mediator {


    public static void main(String[] args) {

        MinesweeperModel model = new MinesweeperModel();
        MinesweeperController controller = new MinesweeperController(model);
        MinesweeperView view = new MinesweeperView(controller);
        model.setListener(view);
        controller.initialize();
    }
}
