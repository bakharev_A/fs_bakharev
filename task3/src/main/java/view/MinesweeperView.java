package view;


import controller.LevelsActionListener;
import controller.MinesweeperController;
import model.MinesweeperCell;
import model.MinesweeperListener;
import setting.Levels;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;


import javax.swing.*;

public class MinesweeperView implements MinesweeperListener {

    private int line;
    private int column;

    private ImageIcon opened;
    private ImageIcon unOpened;
    private ImageIcon mine;
    private ImageIcon flag;
    private ImageIcon one;
    private ImageIcon two;
    private ImageIcon three;
    private ImageIcon four;
    private ImageIcon five;
    private ImageIcon six;
    private ImageIcon seven;
    private ImageIcon eight;
    private ImageIcon smile;
    private ImageIcon winSmile;
    private ImageIcon loseSmile;

    private JFrame screen;
    private JPanel mainPanel;
    private JPanel cellField;
    private JButton startButton;
    private JLabel labelTime;
    private JButton[][] buttonField;

    private MinesweeperController controller;

    public MinesweeperView(MinesweeperController controller) {
        this.controller = controller;
        initIcon();
        screen = new JFrame("Сапёр");

    }

    private void initIcon() {
        unOpened = getScaledImage(this.getClass().getResource("/icons/unopened.png"));
        opened = getScaledImage(this.getClass().getResource("/icons/opened.png"));
        mine = getScaledImage(this.getClass().getResource("/icons/bomb.png"));
        flag = getScaledImage(this.getClass().getResource("/icons/flag.png"));
        one = getScaledImage(this.getClass().getResource("/icons/1.png"));
        two = getScaledImage(this.getClass().getResource("/icons/2.png"));
        three = getScaledImage(this.getClass().getResource("/icons/3.png"));
        four = getScaledImage(this.getClass().getResource("/icons/4.png"));
        five = getScaledImage(this.getClass().getResource("/icons/5.png"));
        six = getScaledImage(this.getClass().getResource("/icons/6.png"));
        seven = getScaledImage(this.getClass().getResource("/icons/7.png"));
        eight = getScaledImage(this.getClass().getResource("/icons/8.png"));

        smile = new ImageIcon(this.getClass().getResource("/icons/smile.png"));
        smile = new ImageIcon(smile.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH));

        winSmile = new ImageIcon(this.getClass().getResource("/icons/winFace.png"));
        winSmile = new ImageIcon(winSmile.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH));

        loseSmile = new ImageIcon(this.getClass().getResource("/icons/loseFace.png"));
        loseSmile = new ImageIcon(loseSmile.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH));

    }

    @Override
    public void setScreenSize(int line, int column) {
        screen.dispose();
        screen = new JFrame("Сапёр");
        this.line = line;
        this.column = column;
        initGamePanels();
        initScreen();
    }

    @Override
    public void restartScreen(int line, int column) {
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                buttonField[i][j].setIcon(unOpened);
                buttonField[i][j].setBackground(Color.GRAY);
            }
        }
        startButton.setIcon(smile);
    }


    private void initGamePanels() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        startButton = new JButton(smile);
        startButton.setPreferredSize(new Dimension(40, 40));
        startButton.addActionListener(event -> controller.restartGame());
        c.anchor = GridBagConstraints.PAGE_START;
        mainPanel.add(startButton, c);

        labelTime = new JLabel("TIME: ");
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        mainPanel.add(labelTime, c);
        addCellButtons();
    }

    @Override
    public void setGameTime(String seconds) {
        labelTime.setText("TIME: " + seconds);
    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu game = new JMenu("Игра");

        JMenuItem newGame = new JMenuItem("Новая игра");
        newGame.addActionListener(event -> controller.restartGame());

        JMenuItem records = new JMenuItem("Рекорды");

        records.addActionListener(event -> controller.getRecords());

        JMenuItem exit = new JMenuItem("Выход");
        exit.addActionListener(event -> System.exit(1));

        JMenu difficulty = new JMenu("Сложность");

        JMenuItem low = new JMenuItem("Легкий");
        low.addActionListener((LevelsActionListener) e -> controller.setLevel(Levels.BEGINNER));

        JMenuItem mid = new JMenuItem("Средний");
        mid.addActionListener((LevelsActionListener) e -> controller.setLevel(Levels.MEDIUM));

        JMenuItem expert = new JMenuItem("Сложный");
        expert.addActionListener((LevelsActionListener) e -> controller.setLevel(Levels.EXPERT));

        JMenuItem custom = new JMenuItem("Своя...");
        custom.addActionListener((LevelsActionListener) e -> showCustomGameDialog());


        difficulty.add(low);
        difficulty.add(mid);
        difficulty.add(expert);
        difficulty.add(custom);

        game.add(newGame);
        game.add(difficulty);
        game.add(records);
        game.add(exit);

        menuBar.add(game);
        return menuBar;
    }

    private void showCustomGameDialog() {
        //Создание диалогового окна с настройками своей сложности
        JDialog dialog = new JDialog(screen, "Своя игра", true);

        dialog.setSize(320, 240);
        dialog.setLocationRelativeTo(screen);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JPanel grid1 = new JPanel(new GridLayout(3, 1, 5, 0));

        grid1.add(new JLabel("По горизонтали: "));
        JSpinner spinnerLine = new JSpinner(new SpinnerNumberModel(1, 1, 25, 1));
        grid1.add(spinnerLine);

        grid1.add(new JLabel("По вертикали: "));
        JSpinner spinnerColumn = new JSpinner(new SpinnerNumberModel(1, 1, 25, 1));
        grid1.add(spinnerColumn);

        grid1.add(new JLabel("Количество мин: "));
        JSpinner spinnerMines = new JSpinner(new SpinnerNumberModel(1, 1, 625, 1));
        grid1.add(spinnerMines);

        JPanel grid = new JPanel(new GridLayout(1, 2, 5, 0));
        JButton okButton = new JButton("OK");
        okButton.addActionListener(e1 -> controller.setCustomLevel((Integer) spinnerLine.getValue(), (Integer) spinnerColumn.getValue(),
                (Integer) (spinnerMines.getValue())));
        grid.add(okButton);
        JButton cancelButton = new JButton("Отмена");
        cancelButton.addActionListener(e12 -> dialog.dispose());
        grid.add(cancelButton);


        JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        flow.add(grid);
        JPanel flow1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        flow1.add(grid1);

        Container container = dialog.getContentPane();

        container.add(flow, BorderLayout.SOUTH);
        container.add(flow1, BorderLayout.NORTH);
        dialog.pack();
        dialog.setVisible(true);
    }

    private void addCellButtons() {
        MyMouseAdapter l = new MyMouseAdapter();
        cellField = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        buttonField = new JButton[line][column];
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                buttonField[i][j] = new JButton(unOpened);
                buttonField[i][j].setPreferredSize(new Dimension(21, 21));
                buttonField[i][j].setName(i + " " + j);

                buttonField[i][j].addMouseListener(l);

                gbc.fill = GridBagConstraints.NONE;
                gbc.gridx = i;
                gbc.gridy = j;
                gbc.anchor = GridBagConstraints.CENTER;
                cellField.add(buttonField[i][j], gbc);
            }
        }
    }

    private void initScreen() {
        screen.add(createMenuBar(), BorderLayout.NORTH);
        screen.add(mainPanel, BorderLayout.CENTER);
        screen.add(cellField, BorderLayout.SOUTH);
        screen.setVisible(true);
        screen.setResizable(false);
        screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        screen.pack();
        screen.setLocationRelativeTo(null);
    }

    @Override
    public void getRecord(String record) {
        JOptionPane.showMessageDialog(null, record, "Рекорды", JOptionPane.PLAIN_MESSAGE);
    }

    @Override
    public void setCellMineNearNeighbors(int line, int column, int countNeighbors) {
        ImageIcon icon;
        switch (countNeighbors) {
            case 1:
                icon = one;
                break;
            case 2:
                icon = two;
                break;
            case 3:
                icon = three;
                break;
            case 4:
                icon = four;
                break;
            case 5:
                icon = five;
                break;
            case 6:
                icon = six;
                break;
            case 7:
                icon = seven;
                break;
            case 8:
                icon = eight;
                break;
            default:
                icon = new ImageIcon("question.png");
                break;
        }
        buttonField[line][column].setIcon(icon);
    }

    @Override
    public void WinOperation() {
        controller.stopTimer();
        startButton.setIcon(winSmile);
        String massage = "Все мины обезврежены!";
        String title = "Победа!";
        JOptionPane.showMessageDialog(null, massage, title, JOptionPane.PLAIN_MESSAGE);
    }

    @Override
    public void askNameDialog() {
        JDialog enterNameDialog = new JDialog(screen, "Введи своё имя", true);
        enterNameDialog.setLocationRelativeTo(screen);

        JPanel grid = new JPanel(new GridLayout(2, 1));

        JLabel nameLabel = new JLabel("Имя: ");
        JTextField textField = new JTextField(null, 10);
        grid.add(nameLabel, BorderLayout.SOUTH);
        grid.add(textField, BorderLayout.NORTH);


        JPanel grid1 = new JPanel(new GridLayout(1, 2, 5, 0));
        JButton okButton = new JButton("OK");
        okButton.addActionListener(e -> {
            controller.saveRecords(textField.getText());
            enterNameDialog.dispose();

        });
        grid1.add(okButton);

        JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        flow.add(grid1);
        JPanel flow1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        flow1.add(grid);

        Container container = enterNameDialog.getContentPane();
        container.add(flow, BorderLayout.SOUTH);
        container.add(flow1, BorderLayout.NORTH);
        enterNameDialog.pack();

        enterNameDialog.setVisible(true);
        enterNameDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);


    }

    @Override
    public void GameOverOperation() {
        controller.stopTimer();
        startButton.setIcon(loseSmile);
        String massage = "Ты проиграл!";
        String title = "BOOM!";
        JOptionPane.showMessageDialog(null, massage, title, JOptionPane.PLAIN_MESSAGE);
    }

    @Override
    public void setCellOpened(int column, int line) {
        buttonField[column][line].setIcon(opened);
    }

    @Override
    public void setCellFlag(int line, int column) {
        buttonField[line][column].setIcon(flag);
    }

    @Override
    public void setCellDefault(int line, int column) {
        buttonField[line][column].setIcon(unOpened);
    }

    @Override
    public void setCellMine(int line, int column) {
        buttonField[line][column].setIcon(mine);
        buttonField[line][column].setBackground(Color.RED);
    }

    @Override
    public void openAllMine(MinesweeperCell[][] gameField) {
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                if (gameField[i][j].isMine()) {
                    buttonField[i][j].setIcon(mine);
                }
            }
        }
    }


    private ImageIcon getScaledImage(URL imageString) {
        ImageIcon imageIcon = new ImageIcon(imageString);
        Image image = imageIcon.getImage();
        Image newImage = image.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        imageIcon = new ImageIcon(newImage);
        return imageIcon;
    }


    private class MyMouseAdapter extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            Component component = e.getComponent();
            String name = component.getName();

            int x = Integer.parseInt(name.split(" ")[0]);
            int y = Integer.parseInt(name.split(" ")[1]);

            if (e.getButton() == 1) {
                controller.leftClick(x, y);

            } else {
                controller.rightClick(x, y);
            }
        }
    }

}


