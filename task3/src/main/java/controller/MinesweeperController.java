package controller;

import model.MinesweeperModel;
import setting.Levels;

import static java.lang.Thread.sleep;
import static java.util.concurrent.TimeUnit.NANOSECONDS;


public class MinesweeperController {
    private MinesweeperModel model;
    private long startTime = 0;
    private long stopTime = 0;
    private boolean running = false;
    private Thread timer;
    private Levels level = Levels.BEGINNER;


    public MinesweeperController(MinesweeperModel model) {
        this.model = model;
    }

    public void initialize() {
        model.initialize(level);
    }

    public void restartGame() {
        if (running) {
            stopTimer();
        }
        model.restart();
    }

    public void setLevel(Levels level) {
        this.level = level;
        model.initialize(level);
    }

    public void setCustomLevel(int line, int column, int mine) {
        model.initializeCustomGame(line, column, mine);
    }

    public void getRecords() {
        model.getRecord();
    }

    public void saveRecords(String name) {
        model.setRecord(name);
    }

    public void leftClick(int line, int column) {
        if (!running) {
            startTimer();
        }
        model.openCell(line, column);
    }

    public void rightClick(int line, int column) {
        model.markCell(line, column);
    }

    private void startTimer() {
        this.startTime = System.nanoTime();
        this.running = true;
        timer = new Thread(() -> {
            while (!timer.isInterrupted()) {
                try {
                    sleep(300);
                    model.setTime(getTime());
                } catch (InterruptedException e) {
                    System.out.println("stopTimer");
                    stopTimer();
                }
            }
        });
        timer.start();

    }

    public void stopTimer() {
        this.stopTime = System.nanoTime();
        this.running = false;
        if (!timer.isInterrupted()) {
            timer.interrupt();
        }
    }

    private long getTime() {
        long elapsed;
        if (running) {
            elapsed = (System.nanoTime() - startTime);
        } else {
            elapsed = (stopTime - startTime);
        }
        return NANOSECONDS.toSeconds(elapsed);
    }

}

