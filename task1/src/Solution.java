import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    private static final int DOWN_BORDER = 1;
    private static final int TOP_BORDER = 32;
    private static int countColumnFirst;
    private static Integer sizeOfTable;
    private static Integer countOfSymbol = 0;
    private static String countOfMinus;
    private static String columnFirstMinus;


    public static void main(String[] args) {

        System.out.println("Enter size of table [" + DOWN_BORDER + " to " + TOP_BORDER + "]");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            sizeOfTable = Integer.parseInt(reader.readLine());
            if (sizeOfTable >= DOWN_BORDER && sizeOfTable <= TOP_BORDER) {
                initParam();
                initTable();
                printTable();
            } else System.err.println("Entered wrong number");


        } catch (NumberFormatException e) {
            System.out.println("Wrong type, enter the NUMBER from " + DOWN_BORDER + " to " + TOP_BORDER);

        } catch (IOException e) {
            System.err.println("Can't read data, try again");
        }
    }

    private static void separateLine() {
        System.out.printf("%n%s", columnFirstMinus);
        for (int i = 1; i <= sizeOfTable; i++) {
            System.out.printf("%s", "+" + countOfMinus);

        }
    }

    private static void initTable() {
        System.out.printf("%" + countColumnFirst + "s", " ");
        for (int i = 1; i <= sizeOfTable; i++) {
            System.out.printf("%s%" + countOfSymbol + "d", "|", i);

        }
        separateLine();

    }

    private static void initParam() {
        int i = sizeOfTable * sizeOfTable;
        int j = sizeOfTable;
        String minus = "-";
        StringBuilder stringBuilder = new StringBuilder();

        countOfSymbol = String.valueOf(i).length();
        stringBuilder.append(minus.repeat(countOfSymbol));
        countOfMinus = stringBuilder.toString();

        stringBuilder.setLength(0);

        countColumnFirst = String.valueOf(j).length();
        stringBuilder.append(minus.repeat(countColumnFirst));
        columnFirstMinus = stringBuilder.toString();
    }

    private static void printTable() {

        for (int i = 1; i <= sizeOfTable; i++) {
            System.out.printf("%n%" + countColumnFirst + "d", i);

            for (int j = 1; j <= sizeOfTable; j++) {
                System.out.printf("|%" + countOfSymbol + "d", i * j);
            }
            separateLine();
        }
    }
}