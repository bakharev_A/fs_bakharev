import lombok.extern.slf4j.Slf4j;

import java.util.*;


@Slf4j
public class Factory {

    public static void main(String[] args) {
        Factory factory = new Factory();
        factory.runFactory();
    }

    private final Object LOCK = new Object();
    private int limit;
    private int countOfProducer;
    private int countOfConsumer;
    private int produceTime;
    private int consumeTime;

    private static volatile int value = 0;
    private Queue<Integer> queue;


    private List<Thread> producerList = new ArrayList<>();
    private List<Thread> consumerList = new ArrayList<>();

    private Factory() {
        loadConfig();
    }

    private void loadConfig() {
        queue = new LinkedList<>();
        Properties properties = new ConfigLoader().getConfig();
        countOfProducer = Integer.parseInt(properties.getProperty("countOfProducer"));
        produceTime = Integer.parseInt(properties.getProperty("produceTime"));
        countOfConsumer = Integer.parseInt(properties.getProperty("countOfConsumer"));
        consumeTime = Integer.parseInt(properties.getProperty("consumeTime"));
        limit = Integer.parseInt(properties.getProperty("limit"));
    }

    private void runFactory() {
        for (Thread p : createProducers()) {
            p.start();
        }
        for (Thread c : createConsumer()) {
            c.start();
        }
    }

    private List<Thread> createProducers() {
        for (int i = 1; i <= countOfProducer; i++) {
            producerList.add(new Thread(new Producer(i, produceTime, this)));
        }
        return producerList;
    }

    private List<Thread> createConsumer() {
        for (int i = 1; i <= countOfConsumer; i++) {
            consumerList.add(new Thread(new Consumer(i, consumeTime, this)));
        }
        return consumerList;
    }

    void produce(int id, int time) throws InterruptedException {
        synchronized (LOCK) {
            while (queue.size() == limit) {
                LOCK.wait();
            }
            queue.offer(value);
            log.info("Producer[{}] ��������: {}", id, value);
            value++;
            LOCK.notify();
        }
        Thread.sleep(time);
    }

    void consume(int id, int time) throws InterruptedException {
        synchronized (LOCK) {
            while (queue.size() == 0) {
                LOCK.wait();
            }
            int value = queue.poll();
            log.info("Consumer[{}] ����: {} ", id, value);
            LOCK.notifyAll();
        }
        Thread.sleep(time);
    }
}