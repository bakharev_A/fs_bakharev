import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Producer implements Runnable {

    private Factory factory;

    private int time;
    private int id;

    Producer(int id, int time, Factory factory) {
        this.id = id;
        this.time = time;
        this.factory = factory;
    }

    @Override
    public void run() {
        try {
            while (true) {
                factory.produce(this.id, this.time);
            }
        } catch (InterruptedException e) {
            log.error("can't produce resource");
        }
    }
}
