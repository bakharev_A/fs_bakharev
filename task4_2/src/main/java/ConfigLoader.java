import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
class ConfigLoader {

    Properties getConfig() {
        Properties prop = new Properties();
        String file = this.getClass().getResource("/config.properties").getFile();
        try {
            FileInputStream fis = new FileInputStream(file);
            prop.load(fis);
        } catch (IOException e) {
            log.error("can't read properties");
        }
        return prop;
    }
}
