import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Consumer implements Runnable {


    private int time;
    private int id;


    private Factory factory;

    Consumer(int id, int time, Factory factory) {
        this.id = id;
        this.time = time;
        this.factory = factory;
    }

    @Override
    public void run() {
        try {
            while (true) {
                factory.consume(this.id, this.time);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
