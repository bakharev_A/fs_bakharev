package main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Solution {

    private final static int NUMBER = 100_000_000;
    private final static int COUNT_OF_THREAD = Runtime.getRuntime().availableProcessors();
    private final static List<Block> blockList = new ArrayList<>();
    private final static List<Task> taskList = new ArrayList<>();
    private static Double result = 0.0;

    public static void main(String[] args) {

        Solution solution = new Solution();
        solution.initBlocks(NUMBER);
        solution.initTask();

        ExecutorService executorService = Executors.newFixedThreadPool(COUNT_OF_THREAD);
        List<Future<Double>> submit = new ArrayList<>();
        for (Task t : taskList) {
            submit.add(executorService.submit(t));
        }

        for (Future<Double> d : submit) {
            try {
                result += d.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println(1 + result);

        executorService.shutdown();
    }

    private void initBlocks(int num) {
        for (int i = 0; i < COUNT_OF_THREAD; i++) {
            int start = (num / COUNT_OF_THREAD) * i + 1;
            int end = (num / COUNT_OF_THREAD) * (i + 1);
            blockList.add(new Block(i, start, end));
        }

        if ((num % COUNT_OF_THREAD) != 0) {
            int ost = num - num % COUNT_OF_THREAD;
            blockList.add(new Block(blockList.size() + 1, ost + 1, num));
        }
    }

    private void initTask() {
        for (int i = 0; i < blockList.size(); i++) {
            taskList.add(new Task(i, blockList.get(i)));
        }
    }
}


