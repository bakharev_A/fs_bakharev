package main;

import java.util.concurrent.Callable;

public class Task implements Callable<Double> {

    private int id;
    private Block block;

    Task(int id, Block block) {
        this.id = id;
        this.block = block;
    }

    @Override
    public Double call()  {
        return calculateBlock(block);
    }

    private Double calculateBlock(Block block) {
        double a = 0;
        for (int i = block.start; i < block.end; i++) {
            a += 1 / Math.pow(2, i);
        }
        return a;
    }

}

