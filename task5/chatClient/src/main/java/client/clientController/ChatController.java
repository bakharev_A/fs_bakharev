package client.clientController;

import client.clientModel.Logic;
import commonLib.chatException.ChatEmptyFormException;
import commonLib.chatException.ChatException;
import commonLib.chatException.ChatNameException;



public class ChatController {

    private Logic logic;

    public ChatController(Logic logic) {
        this.logic = logic;
    }

    public void runChat() {
        logic.runChat();
    }

    public void tryToConnect(String ip, int port, String name) throws ChatNameException, ChatException, ChatEmptyFormException {
        if (ip.isEmpty() || name.isEmpty()) {
            throw new ChatEmptyFormException();
        } else
            logic.connect(ip, port, name);
        new Thread(logic).start();

    }

    public void send(String msg) throws ChatException {
        logic.sendMessage(msg);
    }

    public boolean isNumber(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
