package client.clientView;

import client.clientController.ChatController;
import client.clientModel.ChatListener;
import commonLib.Message;
import commonLib.chatException.ChatException;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Slf4j
public class ChatView implements ChatListener, ActionListener {
    private ChatController controller;
    private JTextField messageInput;
    private JTextArea textBox;
    private JTextArea usersBox;
    private JButton joinChat;


    ChatView() {
    }

    public ChatView(ChatController controller) {
        this.controller = controller;
    }

    private void createView() {
        JFrame chatWindow = new JFrame("Chat");
        chatWindow.setDefaultCloseOperation(chatWindow.EXIT_ON_CLOSE);

        chatWindow.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.insets = new Insets(0, 3, 52, 3);
        chatWindow.add(leftPane(), constraints);

        constraints.insets = new Insets(4, 0, 4, 2);
        chatWindow.add(rightPane(), constraints);

        chatWindow.pack();
        chatWindow.setResizable(false);
        chatWindow.setLocationRelativeTo(null);
        chatWindow.setVisible(true);
    }

    // Панель пользователей и кнопки join
    private JPanel leftPane() {
        JPanel left = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        constraints.gridx = 0;
        constraints.gridy = 0;
        left.add(addJoinBtn(), constraints);

        constraints.ipady = 150;
        constraints.gridx = 0;
        constraints.gridy = 1;
        left.add(addUsersBox(), constraints);

        return left;
    }

    //Паненль чата
    private JPanel rightPane() {
        JPanel right = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.ipady = 200;
        constraints.gridx = 0;
        constraints.gridy = 0;
        right.add(addTextBox(), constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.ipady = 0;
        constraints.gridx = 0;
        constraints.gridy = 1;
        right.add(addSendMsgPanel(), constraints);

        return right;
    }

    // Поле всех сообщений
    private Component addTextBox() {
        textBox = new JTextArea();
        JScrollPane textBoxScrollPane = new JScrollPane(textBox);
        textBoxScrollPane.setWheelScrollingEnabled(true);
        textBox.setLineWrap(true);
        textBox.setWrapStyleWord(true);
        textBox.setEditable(false);

        Font font = textBox.getFont();
        float size = font.getSize();
        textBox.setFont(font.deriveFont(size + 2.0f));

        return textBoxScrollPane;
    }

    // Панель ввода и кнопки send
    private JPanel addSendMsgPanel() {
        JPanel panelMessage = new JPanel();
        panelMessage.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.FIRST_LINE_END;
        panelMessage.add(addMessageInput());
        panelMessage.add(addSendButton());

        return panelMessage;
    }

    // Поле ввода сообщений
    private Component addMessageInput() {
        messageInput = new JTextField(35);
        messageInput.addActionListener(this);
        JScrollPane messageInputScrollPane = new JScrollPane(messageInput);
        messageInputScrollPane.setLocation(0, 0);

        return messageInputScrollPane;
    }

    // Кнопка отправки сообщения
    private JButton addSendButton() {
        JButton sendButton = new JButton("Send");
        sendButton.setToolTipText("Send message");
        sendButton.addActionListener(this);

        return sendButton;
    }

    // Поле пользователей
    private Component addUsersBox() {
        usersBox = new JTextArea();
        usersBox.setLineWrap(true);
        usersBox.setWrapStyleWord(true);
        usersBox.setEditable(false);
        JScrollPane usersBoxScrollPane = new JScrollPane(usersBox);
        usersBoxScrollPane.setLocation(0, 0);

        return usersBoxScrollPane;
    }

    // Кнопка подключения к чату
    private Component addJoinBtn() {
        joinChat = new JButton("Join");
        joinChat.addActionListener(e -> new LoginWindow(controller));

        return joinChat;
    }

    @Override
    public void fillUsersBox(String[] arr) {
        SwingUtilities.invokeLater(() -> {
            for (String s : arr) {
                usersBox.append(s + System.lineSeparator());
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String msg = messageInput.getText();
        if (!msg.isEmpty()) {
            try {
                controller.send(msg);
                messageInput.setText("");
            } catch (ChatException e1) {
                log.debug("Server not available, message has not been sent");
                showInformationWindow("You are not connected to the server, click \"Join\" to connect");
            }
        }
    }

    @Override
    public void addUserToChatList(String text) {
        usersBox.append(text);
        usersBox.append(System.lineSeparator());
    }

    @Override
    public void addUserMessage(Message m) {
        SwingUtilities.invokeLater(() -> {
            String sb = m.getTime() + m.getAuthor() + ": " + m.getText() + System.lineSeparator();
            textBox.append(sb);
        });
    }

    @Override
    public void removeUser(String user) {
        SwingUtilities.invokeLater(() -> {
            String text = usersBox.getText();
            String regex = user + System.lineSeparator();
            text = text.replaceAll(regex, "");
            log.debug("deleted user: {}", text);
            usersBox.setText(text);
        });

    }

    @Override
    public void showUserJoined(String text) {
        textBox.append("User joined: " + text);
        textBox.append(System.lineSeparator());
    }

    @Override
    public void showUserLeft(String text) {
        textBox.append("User leaved: " + text);
        textBox.append(System.lineSeparator());
    }


    @Override
    public void showInformationWindow(String text) {
        JOptionPane.showMessageDialog(null, text);
    }

    @Override
    public void runChat() {
        new Thread(this::createView).start();
    }

    @Override
    public void addInformationToChat(String text) {
        SwingUtilities.invokeLater(() -> textBox.append(text + System.lineSeparator()));
    }

    @Override
    public void showNameWindow(String ip, int port) {
        new NameWindow(ip, port, controller);
    }

    @Override
    public void clearUserField() {
        SwingUtilities.invokeLater(() -> usersBox.setText(""));
    }

    @Override
    public void disabledJoinButton() {
        joinChat.setEnabled(false);
    }
}
