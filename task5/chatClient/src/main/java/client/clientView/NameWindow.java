package client.clientView;

import client.clientController.ChatController;
import commonLib.chatException.ChatEmptyFormException;
import commonLib.chatException.ChatException;
import commonLib.chatException.ChatNameException;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

@Slf4j
class NameWindow extends ChatView {



    NameWindow(String ip, int port, ChatController controller) {

        JFrame nameFrame = new JFrame("Choose new name");
        JTextField name = new JTextField(15);
        JButton ok = new JButton("OK");
        ok.addActionListener(e -> {
            try {
                controller.tryToConnect(ip, port, name.getText());
                nameFrame.dispose();
            } catch (ChatNameException e1) {
                showInformationWindow("Nick already in use, choose another name");
            } catch (ChatException e1) {
                log.debug("Reconnect failed");
            } catch (ChatEmptyFormException e1) {
                showInformationWindow("Field NAME must be filled");
            }
        });

        JPanel panel1 = new JPanel(new FlowLayout());
        panel1.add(new JLabel("NAME:"));
        panel1.add(name);
        JPanel panel2 = new JPanel(new FlowLayout());
        panel2.add(ok);

        nameFrame.setLayout(new GridLayout(2, 1));
        nameFrame.add(panel1);
        nameFrame.add(panel2);
        nameFrame.pack();

        nameFrame.setResizable(false);
        nameFrame.setLocationRelativeTo(null);
        nameFrame.setVisible(true);
    }

}
