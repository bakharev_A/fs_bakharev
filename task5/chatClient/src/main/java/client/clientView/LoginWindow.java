package client.clientView;

import client.clientController.ChatController;
import commonLib.chatException.ChatEmptyFormException;
import commonLib.chatException.ChatException;
import commonLib.chatException.ChatNameException;
import commonLib.chatException.ChatPortException;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

@Slf4j
class LoginWindow extends ChatView {

    private JFrame loginFrame;
    private JTextField ipField;
    private JTextField nickField;
    private JTextField portField;


    LoginWindow(ChatController controller) {
        loginFrame = new JFrame();
        loginFrame.setSize(200, 200);
        ipField = new JTextField(15);
        portField = new JTextField(15);
        nickField = new JTextField(15);
        JLabel ip = new JLabel(" IP: ", SwingConstants.RIGHT);
        JLabel port = new JLabel("PORT: ", SwingConstants.RIGHT);
        JLabel nick = new JLabel(" NAME: ", SwingConstants.RIGHT);
        JButton okButton = new JButton("OK");
        okButton.setSize(new Dimension(40, 20));

        okButton.addActionListener(e -> {

            try {
                if (portField.getText().isEmpty() || !controller.isNumber(portField.getText())){
                    throw new ChatPortException();
                }
                int p = Integer.parseInt(portField.getText());
                controller.tryToConnect(ipField.getText(), p, nickField.getText());
                loginFrame.dispose();
            } catch (ChatNameException e1) {
                showInformationWindow("Nick already in use, choose another name");
            } catch (ChatException e1) {
                showInformationWindow("Can't connect to the server");
                log.debug("Connect failed");
            } catch (ChatEmptyFormException e1) {
                showInformationWindow("Field IP and NAME must be filled!");
            } catch (ChatPortException e1) {
                showInformationWindow("Port must be number");
            }
        });

        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        panel1.add(ip);
        panel1.add(ipField);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        panel2.add(port);
        panel2.add(portField);

        JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        panel3.add(nick);
        panel3.add(nickField);

        JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        panel4.add(okButton);

        loginFrame.setLayout(new GridLayout(4, 1));
        loginFrame.add(panel1);
        loginFrame.add(panel2);
        loginFrame.add(panel3);
        loginFrame.add(panel4);

        loginFrame.pack();

        loginFrame.setResizable(false);
        loginFrame.setLocationRelativeTo(null);
        loginFrame.setAlwaysOnTop(false);
        loginFrame.setVisible(true);
    }
}
