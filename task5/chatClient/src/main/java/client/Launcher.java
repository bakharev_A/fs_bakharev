package client;

import client.clientController.ChatController;
import client.clientModel.Logic;
import client.clientView.ChatView;

public class Launcher {
    public static void main(String[] args)  {

        Logic logic = new Logic();
        ChatController chatController = new ChatController(logic);
        ChatView chatView = new ChatView(chatController);
        logic.setListener(chatView);

        chatController.runChat();


    }
}
