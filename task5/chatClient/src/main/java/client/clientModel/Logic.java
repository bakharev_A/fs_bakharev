package client.clientModel;


import commonLib.Commands;
import commonLib.Message;
import commonLib.chatException.ChatException;
import commonLib.chatException.ChatNameException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static java.lang.Thread.sleep;


@Slf4j
public class Logic implements Runnable {

    private int cPort;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private String myName;

    private ChatListener listener;
    private String ip;

    public void setListener(ChatListener listener) {
        this.listener = listener;
    }


    @Override
    public void run() {
        while (true) {
            try {
                Message m = (Message) in.readObject();
                switch (m.getCommand()) {
                    case OK:
                        listener.showUserJoined(myName);
                        break;
                    case JOIN:
                        listener.addUserToChatList(m.getText());
                        listener.showUserJoined(m.getText());
                        break;
                    case LEAVE:
                        listener.removeUser(m.getText());
                        listener.showUserLeft(m.getText());
                        break;
                    case SENDMESSAGE:
                        listener.addUserMessage(m);
                        break;
                }
            } catch (IOException | ClassNotFoundException e) {
                log.debug("Can't connect to the server");
                listener.clearUserField();
                reConnect();
            }

        }
    }


    public void connect(String address, int port, String userName) throws ChatNameException, ChatException {
        try {
            this.myName = userName;
            this.ip = address;
            cPort = port;
            Socket socket = new Socket(ip, cPort);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());

            Message message = new Message(Commands.JOIN, myName);
            out.writeObject(message);
            out.flush();

            Message m = (Message) in.readObject();
            if (m.getCommand() == Commands.ERROR) {
                log.error(m.getText());
                throw new ChatNameException();
            }

            m = (Message) in.readObject();
            if (m.getCommand() != Commands.USERLIST) {
                log.warn("Wrong command: {}, command should be \"USERLIST\"", m.getCommand());
            }

            String[] userNames = m.getText().split(Message.getUsersDelimiter());
            listener.fillUsersBox(userNames);
            listener.showUserJoined(myName);
            listener.disabledJoinButton();
        } catch (IOException | ClassNotFoundException e) {
            log.debug("Connection to server failed");
            throw new ChatException();
        }
    }

    private void reConnect() {
        listener.addInformationToChat("Connection to server failed");
        listener.addInformationToChat("Try to connect again...");
        for (int i = 0; i < 100; i++) {
            try {
                sleep(2000);
                listener.addInformationToChat("Connection attempt[" + i + "] failed...");
                connect(ip, cPort, myName);
                break;
            } catch (InterruptedException ex) {
                log.error("Exception {}", ex);
            } catch (ChatNameException ex) {
                listener.showInformationWindow("Your previous nickname was taken by someone else, " +
                        "please choose another name.");
                listener.showNameWindow(ip, cPort);
            } catch (ChatException e) {
                log.debug("reconnect");
            }
        }
    }

    public void sendMessage(String msg) throws ChatException {
        if (out == null) {
            log.debug("User try send message without connection to the server");

            throw new ChatException();
        } else {
            try {
                Message message = new Message(Commands.SENDMESSAGE, myName, msg);
                out.writeObject(message);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void runChat() {
        listener.runChat();
    }

}
