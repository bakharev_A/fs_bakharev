package client.clientModel;

import commonLib.Message;

public interface ChatListener {

    void runChat();
    void showUserJoined(String text);
    void showUserLeft(String text);
    void addUserToChatList(String text);
    void addUserMessage(Message m);
    void fillUsersBox(String[] arr);
    void removeUser(String user);
    void showInformationWindow(String text);
    void addInformationToChat(String text);
    void showNameWindow(String ip, int port);
    void clearUserField();
    void disabledJoinButton();
}
