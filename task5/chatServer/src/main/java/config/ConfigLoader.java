package config;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Properties;

@Slf4j
public class ConfigLoader {

   public Properties getConfig() {
        Properties prop = new Properties();
        try {
            prop.load(this.getClass().getResourceAsStream("/config.properties"));
        } catch (IOException e) {
            log.error("can't read properties");
        }
        return prop;
    }
}

