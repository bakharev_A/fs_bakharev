package server;

import commonLib.*;
import commonLib.Message;
import commonLib.chatException.ChatNameException;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static commonLib.Commands.*;

@Slf4j
public class ConnectionThread extends Thread {
    private String name;

    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    private static final ConcurrentMap<String, ConnectionThread> users = new ConcurrentHashMap<>();


    ConnectionThread(Socket socket) throws IOException {
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        while (true) {
            try {
                Message m = (Message) in.readObject();
                switch (m.getCommand()) {
                    case JOIN:
                        processingIncomingJoinMessage(m, this);
                        break;
                    case SENDMESSAGE:
                        sendMessageForAll(m);
                        break;
                    case LEAVE:
                        sendMessageForAll(new Message(LEAVE, name));
                        break;
                }
            } catch (ChatNameException e) {
                log.debug("The user is trying to connect with the already occupied nickname");
            } catch (IOException | ClassNotFoundException e) {
                removeUser(name);
                sendMessageForAllExcludeMe(new Message(LEAVE, name));
                log.debug("User disconnected: {}", name);
                break;
            }
        }
    }

    private void processingIncomingJoinMessage(Message m, ConnectionThread connectionThread) throws IOException, ChatNameException {
        synchronized (users) {
            if (users.containsKey(m.getText())) {
                out.writeObject(new Message(Commands.ERROR, "Name already in use, choose another name"));
                throw new ChatNameException();
            } else {
                name = m.getText();
                users.put(name, connectionThread);
                out.writeObject(new Message(Commands.OK));
                StringBuilder sb = new StringBuilder();
                users.forEach((k, v) -> sb.append(k).append(Message.getUsersDelimiter()));
                out.writeObject(new Message(Commands.USERLIST, sb.toString()));
                sendMessageForAllExcludeMe(new Message(JOIN, name));
            }
        }
    }

    private void removeUser(String name) {
        synchronized (users) {
            users.remove(name);
        }
    }

    private void sendMessageForAllExcludeMe(Message message) {
        users.forEach((k, v) -> {
            if (this != v) v.sendMessage(message);
        });
    }

    private void sendMessageForAll(Message message) {
        users.forEach((k, v) -> v.sendMessage(message));
    }


    private void sendMessage(Message message) {
        try {
            Message msg = new Message(message.getCommand(), message.getAuthor(), message.getText(), message.getTime());
            out.writeObject(msg);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
