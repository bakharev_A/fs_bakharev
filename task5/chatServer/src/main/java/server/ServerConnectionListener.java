package server;

import config.ConfigLoader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ServerConnectionListener {

    private static int port;

    public static void main(String[] args) throws InterruptedException {

        loadConfig();
        log.debug("Server start... port: {}", port);
        while (true) {
            try (ServerSocket serverSocket = new ServerSocket(port)) {
                Socket socket = serverSocket.accept();
                new ConnectionThread(socket).start();
            } catch (IOException e) {
                log.error("Port {} already in use", port);
            }
            TimeUnit.SECONDS.sleep(1);
        }
    }

   private static void loadConfig() {
       Properties p = new ConfigLoader().getConfig();
       port = Integer.parseInt(p.getProperty("Port"));
    }
}
