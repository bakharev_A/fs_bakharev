package commonLib;

import java.io.Serializable;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;


public class Message implements Serializable {
    private String time;
    private Commands command;
    private String author;
    private String text;


    public Message(Commands command) {
        this.command = command;
    }


    public Message(Commands command, String text) {
        this.command = command;
        this.text = text;
    }

    public Message(Commands command, String author, String text) {
        this.command = command;
        this.author = author;
        this.text = text;
    }

    public Message(Commands command, String author, String text, String time) {
        this.command = command;
        this.author = author;
        this.text = text;
        this.time = time;
    }

    public String getTime() {
        LocalTime date = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM);
        time = date.format(formatter);
        return "[" + time + "]: ";
    }


    public Commands getCommand() {
        return command;
    }

    public String getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }



    public static String getUsersDelimiter() {
        return "::";
    }
}
