package figure;

import lombok.extern.slf4j.Slf4j;
import myException.FigureParametersException;

@Slf4j
public class Rectangle extends Figure {

    private double length;
    private double width;
    private double diagonal;


    public Rectangle(String[] readData) throws FigureParametersException {
        super.initFigure(readData);
        log.info("Try create new Rectangle()");
        if (figureParam.size() < 2) {
            log.error("Wrong count of parameters: {}", figureParam.size());
            throw new FigureParametersException();
        } else {
            if (figureParam.get(0) >= figureParam.get(1)) {
                length = figureParam.get(0);
                width = figureParam.get(1);

            } else {
                length = figureParam.get(1);
                width = figureParam.get(0);
            }
            initFigure(readData);
            log.info("Create new Rectangle successful");
        }
    }

    @Override
    void initFigure(String[] readData) {
        type = "Прямоугольник";
        area = calculateArea();
        perimeter = calculatePerimeter();
        diagonal = calculateDiagonal();


    }

    double calculateArea() {
        return length * width;
    }

    double calculatePerimeter() {
        return 2.0 * (length + width);
    }

    double calculateDiagonal() {
        diagonal = Math.sqrt(length * length + width * width);
        return diagonal;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Длина диагонали: " + nf.format(diagonal) + Figure.units + "\n" +
                "Длина: " + nf.format(length) + Figure.units + "\n" +
                "Ширина: " + nf.format(width) + Figure.units;
    }
}
