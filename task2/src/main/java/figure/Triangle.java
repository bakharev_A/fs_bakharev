package figure;

import lombok.extern.slf4j.Slf4j;
import myException.FigureParametersException;

@Slf4j
public class Triangle extends Figure {
    private static final char rad = 176;

    private double ab;
    private double bc;
    private double ca;
    private double alphaAB;
    private double betaCA;
    private double gammaBC;


    public Triangle(String[] readData) throws FigureParametersException {
        super.initFigure(readData);
        log.info("Try create new Triangle()");
        if (figureParam.size() < 3) {
            log.error("Wrong count of parameters: {}", figureParam.size());
            throw new FigureParametersException();
        } else {
            initFigure(readData);
            log.info("Create new Triangle successful");
        }
    }

    @Override
    void initFigure(String[] readData) throws FigureParametersException {
        ab = figureParam.get(0);
        bc = figureParam.get(1);
        ca = figureParam.get(2);
        if (!(((ab + bc) > ca) && ((bc + ca) > ab) && ((ab + ca) > bc))) {
            log.error("Wrong parameters, the sum of the two sides of the triangle must be greater than thirds");
            throw new FigureParametersException();
        } else {
            type = "Треугольник";
            perimeter = calculatePerimeter();
            area = calculateArea();
            alphaAB = anglesAB();
            gammaBC = anglesBC();
            betaCA = anglesCA();
        }
    }

    double calculatePerimeter() {
        return (ab + bc + ca);
    }

    double calculateArea() {
        double halfPerimeter = perimeter / 2;
        return Math.sqrt(halfPerimeter * ((halfPerimeter - ab) * (halfPerimeter - bc) * (halfPerimeter - ca)));
    }

    double anglesAB() {
        return Math.toDegrees(Math.acos((ab * ab + bc * bc - ca * ca) / (2 * ab * bc)));
    }

    double anglesBC() {
        return Math.toDegrees(Math.acos((bc * bc + ca * ca - ab * ab) / (2 * bc * ca)));

    }

    double anglesCA() {
        return Math.toDegrees(Math.acos((ab * ab + ca * ca - bc * bc) / (2 * ab * ca)));
    }


    @Override
    public String toString() {
        String dl = "Длина стороны: ";
        String angle = " Противолежащий угол: ";
        return super.toString() +
                dl + nf.format(ab) + " " + Figure.units + angle + (nf.format(gammaBC) + rad) + "\n" +
                dl + nf.format(bc) + " " + Figure.units + angle + (nf.format(betaCA) + rad) + "\n" +
                dl + nf.format(ca) + " " + Figure.units + angle + (nf.format(alphaAB) + rad);
    }
}
