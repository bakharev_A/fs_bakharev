package figure;

import lombok.extern.slf4j.Slf4j;
import myException.FigureParametersException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

@Slf4j
public abstract class Figure {


    String type;
    static final String units = "мм";
    static final NumberFormat nf = new DecimalFormat("#.##");
    ArrayList<Double> figureParam = new ArrayList<>();
    double area;
    double perimeter;

    void initFigure(String[] readData) throws FigureParametersException {

        Double d;
        if (readData.length > 1) {
            for (int i = 1; i < readData.length; i++) {
                if (isDouble(readData[i])) {
                    d = Double.parseDouble(readData[i]);
                    if (d > 0) {
                        figureParam.add(d);
                    } else {
                        log.error("Figure parameters should be positive");
                        throw new FigureParametersException();
                    }
                } else {
                    log.error("Error parsing double");
                    throw new FigureParametersException();
                }
            }
        } else {
            log.error("Empty parameters");
            throw new FigureParametersException();
        }
    }

    private boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
        } catch (NullPointerException | NumberFormatException e) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "Тип фигуры: " + type + "\n" +
                "Площадь: " + nf.format(area) + "кв." + Figure.units + "\n" +
                "Периметр: " + nf.format(perimeter) + Figure.units + "\n" ;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Figure;
    }
}