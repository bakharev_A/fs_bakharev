package figure;

import lombok.extern.slf4j.Slf4j;
import myException.FigureParametersException;

@Slf4j
public class Circle extends Figure {

    private double radius;
    private double diameter;

    public Circle(String[] readData) throws FigureParametersException {
        log.info("Try create new Circle()");
        initFigure(readData);
        log.info("Create new Circle successful");
    }

    @Override
    void initFigure(String[] readData) throws FigureParametersException {
        super.initFigure(readData);
        type = "Круг";
        radius = figureParam.get(0);
        diameter = calculateDiameter();
        area = calculateArea();
        perimeter = calculatePerimeter();
    }


    double calculateDiameter() {
        return radius * 2;
    }

    double calculateArea() {
        return Math.PI * (radius * radius);
    }

    double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }


    @Override
    public String toString() {
        return super.toString() +
                "Радиус: " + nf.format(radius) + Figure.units + "\n" +
                "Диаметр: " + nf.format(diameter) + Figure.units;

    }

}
