package myException;

public class FigureParametersException extends Exception {
    public FigureParametersException() {
        super();
    }

    public FigureParametersException(String message) {
        super(message);

    }
}

