package main;

import lombok.extern.slf4j.Slf4j;
import myException.FigureParametersException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Slf4j
public class Main {

    public static void main(String[] args) {


        try {
            System.out.println("Enter file name or path: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            String[] readData = Reader.readFile(reader.readLine());
            Solution solution = new Solution(readData);

            System.out.println("Print to console? (Y/N):");

            String answer = reader.readLine();
            reader.close();

            if (answer.equalsIgnoreCase("y")) {
                solution.print();
            } else {
                solution.write();
            }
        } catch (FigureParametersException e) {
            log.error("Incorrect figure parameters");
        } catch (IOException e) {
            log.error("File not found");
        }
    }
}

