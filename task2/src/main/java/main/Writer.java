package main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Paths;

class Writer {
    private static final Logger log = LoggerFactory.getLogger(Writer.class.getName());
    private Writer(){}

    static void writeFile(String data) {
        log.info("writeFile() start");
        try {
            Files.write(Paths.get("out.txt"), data.getBytes());
        } catch (IOException e) {
            log.error("Can't write data");

        }
        log.info("writeFile() successful");
    }
}
