package main;

import lombok.extern.slf4j.Slf4j;

import java.io.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
class Reader {
    private Reader() {
    }

    static String[] readFile(String file) throws IOException {
        String[] readData;
        String data;
        log.info("Start read from file");
        data = Files.readString(Paths.get(file), StandardCharsets.UTF_8);
        if (data.isEmpty()) {
            throw new FileNotFoundException();
        } else {
            readData = data.split("\\s+");
            log.info("ReadFile() successful");
            return readData;
        }
    }
}
