package main;


import figure.Circle;
import figure.Figure;
import figure.Rectangle;
import figure.Triangle;


import lombok.extern.slf4j.Slf4j;
import myException.FigureParametersException;

@Slf4j
class Solution {

    private Figure result;

    Solution() {
    }

    Solution(String[] readData) throws FigureParametersException {
        result = init(readData);
    }


    Figure init(String[] readData) throws FigureParametersException {

        String typeFigure = readData[0];

        switch (typeFigure.toUpperCase()) {
            case "CIRCLE":
                result = new Circle(readData);
                break;
            case "RECTANGLE":
                result = new Rectangle(readData);
                break;
            case "TRIANGLE":
                result = new Triangle(readData);
                break;
            default:
                log.error("Incorrect data in first line");
                throw new FigureParametersException();
        }

        return result;
    }

    void write() {
        log.info("write() start");
        Writer.writeFile(result.toString());
        log.info("write() successful");
    }

    void print() {
        log.info("print() start");
        System.out.println(result.toString());
        log.info("print() successful");

    }

}
