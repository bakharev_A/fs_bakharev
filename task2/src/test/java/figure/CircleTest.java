package figure;

import myException.FigureParametersException;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CircleTest {
    private String[] data = new String[]{"Circle", "5"};
    private Circle circle = new Circle(data);

    public CircleTest() throws FigureParametersException {
    }

    @Test
    public void calculateDiameterTest_should_return_diameter() {
        double res = circle.calculateDiameter();
        assertEquals(10, res, 0.01);
    }

    @Test
    public void calculateArea_should_return_area() {
        double res = circle.calculateArea();
        assertEquals(res, Math.PI * (5 * 5), 0.01);
    }

    @Test
    public void calculatePerimeterTest_should_return_perimeter() {
        double res = circle.calculatePerimeter();
        assertEquals(res, 2 * Math.PI * 5, 0.01);
    }
}
