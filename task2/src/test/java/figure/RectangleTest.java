package figure;

import myException.FigureParametersException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class RectangleTest {
    private String[] data = new String[]{"Rectangle", "5", "7"};
    private Rectangle rectangle = new Rectangle(data);

    public RectangleTest() throws FigureParametersException {
    }

    @Test
    public void calculateDiagonal_Should_Return_Diagonal() {
        double res = rectangle.calculateDiagonal();
        assertEquals(Math.sqrt(5 * 5 + 7 * 7), res, 0.01);
    }

    @Test
    public void calculateAreaTest_should_Return_area() {
        double res = rectangle.calculateArea();
        assertEquals(res, 5 * 7, 0.01);
    }

    @Test
    public void calculatePerimeterTest_should_return_perimeter() {
        double res = rectangle.calculatePerimeter();
        assertEquals(res, 2 * (5 + 7), 0.01);
    }
}
