package figure;

import myException.FigureParametersException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TriangleTest {
    private String[] data = new String[]{"Triangle", "5", "7", "11"};
    private Triangle triangle = new Triangle(data);
    private final double ab = Double.parseDouble(data[1]);
    private final double bc = Double.parseDouble(data[2]);
    private final double ca = Double.parseDouble(data[3]);

    private double res;


    public TriangleTest() throws FigureParametersException {
    }

    @Test
    public void anglesAB_Should_Return_anglesAB() {
        res = triangle.anglesAB();
        assertEquals(Math.toDegrees(Math.acos((ab * ab + bc * bc - ca * ca) / (2 * ab * bc))), res, 0.001);
    }

    @Test
    public void anglesBC_Should_Return_anglesBC() {
        res = triangle.anglesBC();
        assertEquals (Math.toDegrees(Math.acos((bc * bc + ca * ca - ab * ab) / (2 * bc * ca))), res, 0.001);
    }

    @Test
    public void anglesCA_Should_Return_anglesCA() {
        res = triangle.anglesCA();
        assertEquals(Math.toDegrees(Math.acos((ab * ab + ca * ca - bc * bc) / (2 * ab * ca))), res, 0.001);
    }

    @Test
    public void calculateArea_should_return_area() {
        res = triangle.calculateArea();
        double halfPerimeter = triangle.perimeter / 2;
        assertEquals(Math.sqrt(halfPerimeter * ((halfPerimeter - ab) * (halfPerimeter - bc) * (halfPerimeter - ca))), res, 0.01);
    }
    @Test
    public void calculatePerimeter_should_return_perimeter(){
        res = triangle.calculatePerimeter();
        assertEquals(ab + bc + ca, res, 0.01);
    }
}
