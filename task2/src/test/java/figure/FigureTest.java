package figure;

import myException.FigureParametersException;
import org.junit.Test;


public class FigureTest {

    private Figure figure;


    @Test(expected = FigureParametersException.class)
    public void initFigure_should_throw_FigureParametersException_when_has_no_numeric_parameters() throws FigureParametersException {
        String[] data = new String[]{"Circle"};
        figure = new Circle(data);
    }

    @Test(expected = NullPointerException.class)
    public void initFigure_should_throw_NullPointerException_when_the_parameters_are_not_numbers() throws FigureParametersException {
        String[] data = new String[]{"Circle", "s"};
        figure.initFigure(data);
    }

}
