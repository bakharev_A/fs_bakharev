package main;


import figure.Circle;
import figure.Figure;
import myException.FigureParametersException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SolutionTest {
    private String[] data = new String[]{"Circle", "5"};
    private Solution solution = new Solution();

    @Test
    public void init_should_return_result_of_new_Figure() throws FigureParametersException {
        Figure figure = new Circle(data);
        assertEquals(figure, solution.init(data));
    }

}
